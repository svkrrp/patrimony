const path = require('path');
const fs = require('fs');
const solc = require('solc');

const patrimonyPath = path.resolve(__dirname, 'contracts', 'Patrimony.sol');
const source = fs.readFileSync(patrimonyPath, 'utf8');

const input = {
  language: 'Solidity',
  sources: {
    'Patrimony.sol': {
      content: source,
    },
  },
  settings: {
    outputSelection: {
      '*': {
        '*': ['*'],
      },
    },
  },
};

module.exports = JSON.parse(solc.compile(JSON.stringify(input))).contracts[
  'Patrimony.sol'
].Patrimony;
