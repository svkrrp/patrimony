import React from 'react'

export default function Nav() {
  return (
    <header id="header" className="header d-flex align-items-center">

    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
      <a href="/" className="logo d-flex align-items-center">
        <h1>Patrimony<span>.</span></h1>
      </a>
      <nav id="navbar" className="navbar">
        <ul>
          <li><a href="/" style={{fontFamily: 'Edu SA Beginner', fontSize: '22px'}}>Home</a></li>
          <li><a href="/buy" style={{fontFamily: 'Edu SA Beginner', fontSize: '22px'}}>See all lands</a></li>
          <li><a href="/register" style={{fontFamily: 'Edu SA Beginner', fontSize: '22px'}}>Register</a></li>
        </ul>
      </nav>

      <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
      <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>

    </div>
  </header>
  )
}
