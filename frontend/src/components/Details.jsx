import React, { useEffect, useState } from "react";
import web3 from "../web3";
import { ABI, ABI_ADDRESS, INCORRECT_VALUE_FOR_LAND_IMAGE_URL, DEFAULT_LAND_IAMGE_URL } from "../helpers/constants";
import { useParams } from "react-router-dom";
import Stepper from "./Stepper";
import Loader from "./Loader";
import { toTitleCase } from '../helpers/utils';

export default function Details() {
  const [isLoading, setIsLoading] = useState(false);
  const [land, setLand] = useState(null);
  const [name, setName] = useState("");
  const [previousOwners, setPreviousOwners] = useState([]);
  const { id } = useParams();

  useEffect(() => {
    onPageLoadHandler();
  }, []);

  const onPageLoadHandler = async () => {
    setIsLoading(true);
    const patrimony = new web3.eth.Contract(ABI, ABI_ADDRESS);
    const landsCount = await patrimony.methods.landsCount().call();
    const promises = [];
    for (let i = 0; i < landsCount; i++) {
      promises.push(patrimony.methods.lands(i).call());
    }
    const landsArr = await Promise.all(promises);
    const selectedLand = landsArr.find((l) => l.landId == id);
    const index = landsArr.findIndex((l) => l.landId == id);
    setLand(selectedLand);

    const previousOwnersLength = selectedLand.landOwnersLength;
    const previousOwnersPromises = [];
    for (let i = 0; i < previousOwnersLength; i++) {
      previousOwnersPromises.push(
        patrimony.methods.getPreviousLandOwners(index, i).call()
      );
    }
    const previousOwnersArr = await Promise.all(previousOwnersPromises);
    setPreviousOwners(previousOwnersArr);
    setIsLoading(false);
  };

  const onBuyHandler = async () => {
    if (!name) return;
    setIsLoading(true);
    const patrimony = new web3.eth.Contract(ABI, ABI_ADDRESS);
    const accounts = await web3.eth.getAccounts();

    await patrimony.methods.transfer(land.currentOwner, name).send({
      from: accounts[0],
    });
    setIsLoading(false);
  };

  console.log(land, previousOwners);

  if (!land) return null;

  return (
    <>
      {isLoading ? (
        <Loader />
      ) : (
        <div class="container">
          <div class="row justify-content-start p-3">
            <div class="col-auto my-4">
              <img
                src = {!land.imageUrl || INCORRECT_VALUE_FOR_LAND_IMAGE_URL.includes(land.imageUrl.toUpperCase()) ? DEFAULT_LAND_IAMGE_URL : land.imageUrl }
                class="img-thumbnail"
                alt="land image"
                className="row-xs-6"
                style={{ width: "90%", marginTop: "55px", borderRadius : '5px', boxShadow: '5px 5px 5px grey' }}
              ></img>
            </div>
            <div class="col-auto my-4" style={{ paddingLeft: "12px" }}>
              <div className="details row-xs-6">
                <h2
                  className="mt-4 mx-6"
                  style={{ fontFamily: "Pacifico", fontSize: "32px" }}
                >
                  Land Details
                </h2>
                <hr />
                <table className="table table-borderless">
                  <tr>
                    <td style={{ fontFamily: "Pacifico", fontSize: "20px" }}>
                      Current Owner Name
                    </td>
                    <td
                      style={{
                        fontFamily: "Edu SA Beginner",
                        fontSize: "20px",
                      }}
                    >
                      {" "}
                      <strong>
                        {previousOwners[previousOwners.length - 1].name}{" "}
                      </strong>
                    </td>
                  </tr>
                  {/* <tr>
                      <td style={{fontFamily: 'Pacifico', fontSize: '20px'}}>Current Owner Name</td>
                      <td style={{fontFamily: 'Edu SA Beginner', fontSize: '20px'}}> <strong>{land.currentOwner} </strong></td>
                    </tr> */}
                  <tr>
                    <td style={{ fontFamily: "Pacifico", fontSize: "19px" }}>
                      Address Line 1
                    </td>
                    <td
                      style={{
                        fontFamily: "Edu SA Beginner",
                        fontSize: "20px",
                      }}
                    >
                      {" "}
                      <strong>
                        {toTitleCase(land.addr.firstLineAddress)}
                      </strong>
                    </td>
                  </tr>
                  <tr>
                    <td style={{ fontFamily: "Pacifico", fontSize: "19px" }}>
                      Address Line 2
                    </td>
                    <td
                      style={{
                        fontFamily: "Edu SA Beginner",
                        fontSize: "20px",
                      }}
                    >
                      {" "}
                      <strong>
                        {toTitleCase(land.addr.secondLineAddress)}
                      </strong>
                    </td>
                  </tr>
                  <tr>
                    <td style={{ fontFamily: "Pacifico", fontSize: "20px" }}>
                      City
                    </td>
                    <td
                      style={{
                        fontFamily: "Edu SA Beginner",
                        fontSize: "20px",
                      }}
                    >
                      {" "}
                      <strong>{land.addr.city.toString()} </strong>
                    </td>
                  </tr>
                  <tr>
                    <td style={{ fontFamily: "Pacifico", fontSize: "20px" }}>
                      State
                    </td>
                    <td
                      style={{
                        fontFamily: "Edu SA Beginner",
                        fontSize: "20px",
                      }}
                    >
                      <strong>{land.addr.state.toString()}</strong>
                    </td>
                  </tr>
                  <tr>
                    <td style={{ fontFamily: "Pacifico", fontSize: "20px" }}>
                      Pincode
                    </td>
                    <td
                      style={{
                        fontFamily: "Edu SA Beginner",
                        fontSize: "20px",
                      }}
                    >
                      <strong>{land.addr.pincode}</strong>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>

          <hr />

          <h2
            className="my-4"
            style={{ fontFamily: "Pacifico", fontSize: "38px" }}
          >
            {" "}
            Previous owners
          </h2>
          <Stepper previousOwners={previousOwners} />
          <hr />
          <h2
            className="mb-4 mt-4"
            style={{ fontFamily: "Pacifico", fontSize: "38px" }}
          >
            Want to Buy?
          </h2>
          <div style={{display: 'flex', justifyContent: 'space-between'}}>
          <input
            className="form-control mb-4 mt-2"
            type="text"
            placeholder="Enter your name"
            onChange={(e) => setName(e.target.value)}
            style = {{width: '90%'}}
          />
          <button
            className="btn btn-primary px-4 mb-4"
            onClick={onBuyHandler}
            style={{fontFamily: 'Edu SA Beginner', fontSize: '24px'}}
          >
            Buy
          </button>
          </div>
        </div>
      )}
    </>
  );
}
