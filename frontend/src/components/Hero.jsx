import React from 'react'

export default function Hero() {
  return (
    <section id="hero" class="hero">
        <div class="container position-relative">
        <div class="row gy-5" data-aos="fade-in">
            <div class="col-lg-6 order-2 order-lg-1 d-flex flex-column justify-content-center text-center text-lg-start">
            <h2 style={{fontFamily: 'Fjalla One'}}>Welcome to <span>Patrimony</span></h2>
            <p style={{fontFamily: 'Pacifico', fontSize: '24px'}}>One stop to Buy and Sell lands, without paying extra charges</p>
            <div class="d-flex justify-content-center justify-content-lg-start">
                <a href="/register" class="btn-get-started" style={{fontFamily: 'Edu SA Beginner', fontSize: '24px'}}>Get Started</a>
                {/* <a href="https://www.youtube.com/watch?v=LXb3EKWsInQ" class="glightbox btn-watch-video d-flex align-items-center"><i class="bi bi-play-circle"></i><span>Watch Video</span></a> */}
            </div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2">
            <img src="landTransfer.png" class="img-fluid" alt="" data-aos="zoom-out" data-aos-delay="100" />
            </div>
        </div>
        </div>

        <div class="icon-boxes position-relative">
            <div class="container position-relative">
                <div class="row gy-4 mt-5">
                    <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="100">
                        <div class="icon-box">
                        <div class="icon"><i class="bi bi-easel"></i></div>
                        <a href="" class="stretched-link"><h2 class="title" style={{fontFamily: 'Edu SA Beginner', fontSize: '32px'}}>Peer to Peer transfer</h2></a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="200">
                        <div class="icon-box">
                        <div class="icon"><i class="bi bi-gem"></i></div>
                        <a href="" class="stretched-link"><h2 class="title" style={{fontFamily: 'Edu SA Beginner', fontSize: '32px'}}>100% secure</h2></a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="300">
                        <div class="icon-box">
                        <div class="icon"><i class="bi bi-geo-alt"></i></div>
                        <a href="" class="stretched-link"><h2 class="title" style={{fontFamily: 'Edu SA Beginner', fontSize: '32px'}}>No extra or hidden charges</h2></a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6" data-aos="fade-up" data-aos-delay="500">
                        <div class="icon-box">
                        <div class="icon"><i class="bi bi-command"></i></div>
                        <a href="" class="stretched-link"><h2 class="title" style={{fontFamily: 'Edu SA Beginner', fontSize: '32px'}}>Traceable</h2></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  )
}
