import React, { useEffect, useState } from 'react'
import web3 from '../web3';
import { ABI, ABI_ADDRESS, DEFAULT_LAND_IAMGE_URL, INCORRECT_VALUE_FOR_LAND_IMAGE_URL } from "../helpers/constants";
import Loader from './Loader';

const Buy = () => {

    const [isLoading, setIsLoading] = useState(false)
    const [lands, setLands] = useState([])

    useEffect(() => {
        onPageLoadHandler();
    }, [])

    const onPageLoadHandler = async () => {
        setIsLoading(true)
        const patrimony = new web3.eth.Contract(ABI, ABI_ADDRESS);
        const landsCount = await patrimony.methods.landsCount().call()
        const promises = []
        for (let i=0; i<landsCount; i++) {
            promises.push(patrimony.methods.lands(i).call())
        }
        const landsArr = await Promise.all(promises)
        setLands(landsArr)
        setIsLoading(false)
    }

    console.log(lands)

    return (
        <div className='buy'>
            {
                isLoading
                ? <Loader />
                : <div className='d-flex'>
                    {
                        lands.map((land, idx) => <div key={idx} class="container">
                        <section class="mx-auto my-5" style={{maxWidth: '23rem'}}>
                      
                          <div class="card">
                            <div class="card-body d-flex flex-row">
                              <img src="avatar.png" class="rounded-circle me-3" height="50px"
                                width="50px" alt="avatar" />
                              <div>
                                <h5 class="card-title font-weight-bold mb-2">{land.landId}</h5>
                                <p class="card-text"><i class="far fa-clock pe-2"></i>{land.addr.state}</p>
                              </div>
                            </div>
                            <div class="bg-image hover-overlay ripple rounded-0" data-mdb-ripple-color="light">
                              <img class="img-fluid" src={!land.imageUrl || INCORRECT_VALUE_FOR_LAND_IMAGE_URL.includes(land.imageUrl.toUpperCase()) ? DEFAULT_LAND_IAMGE_URL : land.imageUrl }
                                alt="Card image cap" />
                            </div>
                            <div class="card-body">
                              <p class="card-text collapse" id="collapseContent">
                                
                              </p>
                              <div class="d-flex justify-content-between">
                                <a class="btn btn-link link-danger p-md-1 my-1" data-mdb-toggle="collapse" href={`/details/${land.landId}`}
                                  role="button" aria-expanded="false" aria-controls="collapseContent">Read more</a>
                                <div>
                                  <i class="fas fa-share-alt text-muted p-md-1 my-1 me-2" data-mdb-toggle="tooltip"
                                    data-mdb-placement="top" title="Share this post"></i>
                                  <i class="fas fa-heart text-muted p-md-1 my-1 me-0" data-mdb-toggle="tooltip" data-mdb-placement="top"
                                    title="I like it"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                          
                        </section>
                      </div>)
                    }
                </div>
            }
        </div>
    );
};

export default Buy;